def calcular_area(lado):
  area=lado*lado
  return area

if __name__=="__main__":  #con return y argumento
    print("Bienvenido Sr. Cuadrado")
    lado= float(input("lado: "))
    resultado= calcular_area(lado)
    print(resultado)

def saludar():
    print("Bienvenido Sr. Cuadrado")

if __name__=="__main__": #sin return y sin argumento
    saludar()
    lado= float(input("lado: "))
    resultado= calcular_area(lado)
    print(resultado)